import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, TextInput } from 'react-native'
import React, { useState, useEffect } from 'react'
import auth from '@react-native-firebase/auth'
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import TouchID from 'react-native-touch-id';
import Home from '../DashboardScreen';
import { pokeball } from '../../assets/icon';
import { Formik } from 'formik';
import Input from '../../component/input'
import * as Yup from 'yup'
import Register from '../Register';

const Login = ({ navigation }) => {

    // useEffect(() => {
    //     GoogleSignin.configure({
    //         webClientId: '873113659672-10qnfui1em3ekj8hmss6at6t2tdks6s6.apps.googleusercontent.com',
    //     });
    //     const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    //     return subscriber; // unsubscribe on unmount
    // });

    async function onGoogleButtonPress() {
        // Get the users ID token
        const { idToken } = await GoogleSignin.signIn();

        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);

        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    }

    const handleSubmit = ({ email, password }) => {
        auth().signInWithEmailAndPassword(email, password).then((response) => {
            navigation.navigate('Home')
        })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    console.log('That email address is already in use!');
                }

                if (error.code === 'auth/invalid-email') {
                    console.log('That email address is invalid!');
                }
                console.error(error);
            });
    }

    const validationSchema = Yup.object({
        email: Yup
            .string()
            .email("Please enter valid email")
            .required('Email Address is Required'),
        password: Yup
            .string()
            .min(8, ({ min }) => `Password must be at least ${min} characters`)
            .required('Password is required'),
    })

    return (
        <Formik
            validateOnMount={true}
            initialValues={{ email: '', password: '' }}
            onSubmit={(values) => handleSubmit(values)}
            validationSchema={validationSchema}
        >
            {({ handleChange, handleBlur, handleSubmit, values, errors, touched, isValid }) => {
                const { email, password } = values
                return <>
                    <View style={styles.container}>
                        <View style={styles.applicationName}>
                            <ImageBackground source={pokeball} style={styles.logo}></ImageBackground>
                            <Text style={styles.appName}>My Pokemon</Text>
                        </View>
                        <View style={styles.textInputContainer}>
                            <Input style={styles.input} placeholder="Enter Email" onChangeText={handleChange('email')} value={values.email} placeholderTextColor={'black'} onBlur={handleBlur('email')} />
                            {errors.email &&
                                <Text style={{ fontSize: 15, color: 'red', alignSelf: 'center' }}>{errors.email}</Text>
                            }
                            <Input style={styles.input} onChangeText={handleChange('password')} value={values.password} placeholder="Enter Password" placeholderTextColor={'black'} onBlur={handleBlur('password')} />
                            {errors.password &&
                                <Text style={{ fontSize: 15, color: 'red', alignSelf: 'center' }}>{errors.password}</Text>
                            }
                        </View>
                        <View style={styles.loginButton}>
                            <TouchableOpacity onPress={handleSubmit} disabled={!isValid || values.email === ''}>
                                <View style={styles.buttonContainer}>
                                    <Text style={styles.loginText}>Login</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.loginButton}>
                            <TouchableOpacity onPress={() => onGoogleButtonPress().then(() => alert('Signed in with Google!'))}>
                                <View style={styles.buttonContainer}>
                                    <Text style={styles.loginText}>Login With Google</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.registerSign}>
                            <Text>Don't have account ?</Text>
                            <TouchableOpacity>
                                <Text>Register</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </>
            }}
        </Formik>
    )
}

export default Login

const styles = StyleSheet.create({
    applicationName: {
        alignItems: 'center',
        fontSize: 24,
    },
    text: {
        color: 'black',
        fontSize: 22
    },
    container: {
        flex: 1,
        backgroundColor: '#F7F5F2'
    },
    loginImageContainer: {
        alignItems: 'center',
    },
    loginImage: {
        width: 360,
        height: 230,
        marginTop: 80,
        borderRadius: 10,
    },
    textInputContainer: {
        marginTop: 20,
        alignItems: 'center',
        marginBottom: 10
    },
    // input: {
    //     color: 'black',
    //     borderWidth: 1,
    //     width: 330,
    //     height: 40,
    //     borderRadius: 10,
    //     marginBottom: 10
    // },
    buttonContainer: {
        backgroundColor: 'black',
        width: 280,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15
    },
    loginButton: {
        alignItems: 'center',
        marginTop: 12
    },
    registerSign: {
        alignItems: 'center',
        marginTop: 5
    },
    loginText: {
        color: 'white'
    },
    logoutButton: {
        alignItems: 'center',
        marginBottom: 150,
        justifyContent: 'center'
    },
    appName: {
        color: 'black',
        fontSize: 20,
        marginTop: 2
    },
    logo: {
        width: 100,
        height: 100,
        marginTop: 20
    }
})